Flatpak build are processed daily by the continuous integration script from KDE Jenkinks server.

Bundle must be deployed automatically on Kdeapps Flatpak testing source repository.

You can handle the digiKam Flatpak bundle on Linux desktop installer as Plasma Discover or Gnome Software.

- Status;          https://binary-factory.kde.org/view/Flatpak/job/Digikam_flatpak/
- Configuration:   https://invent.kde.org/kde/flatpak-kde-applications/-/blob/master/org.kde.digikam.json
- Repository:      https://distribute.kde.org/kdeapps.flatpakrepo
- Plasma Discover: https://userbase.kde.org/Discover
- Gnome Software:  https://wiki.gnome.org/Apps/Software

[![](https://i.imgur.com/IHxNhDT.png "Settings Kdeapps testings source repository in Plasma Discover Application")](https://imgur.com/IHxNhDT)
[![](https://i.imgur.com/Gu83kFI.png "digiKam Flatpak bundle Properties in Plasma Discover Application")](https://imgur.com/Gu83kFI)
